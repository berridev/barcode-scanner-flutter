import 'package:flutter/material.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';

void main() => runApp(
      MaterialApp(
        home: MyHome(),
        title: "barcode_scanner",
        theme: ThemeData(primarySwatch: Colors.grey),
        debugShowCheckedModeBanner: false,
      ),
    );

class MyHome extends StatefulWidget {
  @override
  _MyHomeState createState() => _MyHomeState();
}

class _MyHomeState extends State<MyHome> {
  String code = "";
  String getCode = "";

  Future scanBarcode() async {
    getCode =
        await FlutterBarcodeScanner.scanBarcode("#009922", "Cancel", true);
    setState(() {
      code = getCode;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFd3f4ff),
      appBar: AppBar(
        title: Text(
          "Barcode Scanner",
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(25.0),
              child: FlatButton(
                color: Colors.yellow,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(40)),
                splashColor: Colors.white,
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text(
                    "Scan Barcode",
                    style: TextStyle(
                        fontSize: 20,
                        color: Colors.black54,
                        fontWeight: FontWeight.bold),
                  ),
                ),
                onPressed: () {
                  scanBarcode();
                },
              ),
            ),
            Text(code),
          ],
        ),
      ),
    );
  }
}
